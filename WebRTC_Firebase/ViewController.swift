//
//  ViewController.swift
//  WebRTC_Firebase
//
//  Created by trying-things on 20/06/2018.
//  Copyright © 2018 trying-things. All rights reserved.
//

import UIKit
import WebRTC

class ViewController: UIViewController {
    
    let webRTC = WebRTC.shared
    
    // Use Firebase.swift to connect to the database.
    let database = Firebase.shared

    override func viewDidLoad() {
        super.viewDidLoad()
        
        webRTC.peerConnectionFactory = RTCPeerConnectionFactory()
        webRTC.startAudio()
        database.setup()
    }

    @IBAction func pressConnect(_ sender: Any) {
        if webRTC.peerConnection == nil {
            print("make offer")
            webRTC.makeOffer()
        } else {
            print("peer already exists")
        }
    }
    
    @IBAction func pressHangUp(_ sender: Any) {
        webRTC.hangUp()
        _ = self.navigationController?.popToRootViewController(animated: true)
    }
    
}

