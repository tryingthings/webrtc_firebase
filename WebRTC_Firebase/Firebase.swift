//
//  Firebase.swift
//  WebRTC_Firebase
//
//  Created by trying-things on 20/06/2018.
//  Copyright © 2018 trying-things. All rights reserved.
//

import UIKit
import Firebase
import SwiftyJSON

class Firebase: NSObject {
    
    public static let shared = Firebase()
    
    var sender: Int = 1
    var receiver: Int = 2
    
    var callReceiver: DatabaseReference?
    var callSender: DatabaseReference?

    func setup() {
        // Add an reference to the database for "Call 2"
        callReceiver = Database.database().reference().child("Call/\(receiver)")
        // Add an reference to the database for "Call 1"
        callSender = Database.database().reference().child("Call/\(sender)")
        // Remove from database when disconnected (ie. hang up, network issue)
        callSender?.onDisconnectRemoveValue()
        negotiateConnection()
    }
    
    func negotiateConnection() {
        // Listen for changes in the database
        callReceiver?.observe(.value, with: { (snapshot) in
            // If no changes are found, do nothing
            guard snapshot.exists() else {
                print("note: negotiateConnection() in Firebase.swift found no snapshot")
                return
            }
            // Print changes to console
            print("note: negotiateConnection() in Firebase.swift found \(snapshot.value ?? "nothing")")
            
            // Convert snapshot to JSON
            let message = JSON(snapshot.value!)
            // Use Swifty_JSON.swift to get the content
            let swiftyJSON = SwiftyJSON()
            swiftyJSON.decodeSnapshot(message)
        })
    }
}
