//
//  SwiftyJSON.swift
//  WebRTC_Firebase
//
//  Created by trying-things on 20/06/2018.
//  Copyright © 2018 trying-things. All rights reserved.
//

import UIKit
import SwiftyJSON
import WebRTC
import Firebase

class SwiftyJSON: NSObject {
    
    let webRTC = WebRTC.shared

    func decodeSnapshot(_ message: JSON) {
        let type = message["type"].stringValue
        switch type {
        case "offer":
            print("Received offer ...")
            let offer = RTCSessionDescription(
                type: RTCSessionDescription.type(for: type),
                sdp: message["sdp"].stringValue)
            webRTC.setOffer(offer)
        case "answer":
            print("Received answer ...")
            let answer = RTCSessionDescription(
                type: RTCSessionDescription.type(for: type),
                sdp: message["sdp"].stringValue)
            webRTC.setAnswer(answer)
        case "candidate":
            print("Received ICE candidate ...")
            let candidate = RTCIceCandidate(
                sdp: message["ice"]["candidate"].stringValue,
                sdpMLineIndex: message["ice"]["sdpMLineIndex"].int32Value,
                sdpMid: message["ice"]["sdpMid"].stringValue)
            webRTC.addIceCandidate(candidate)
        case "close":
            print("Peer closed")
            webRTC.hangUp()
        default:
            return
        }
    }
}
